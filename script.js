let plusArr = ["Рок-н-рол", "Класика", "Реп", "Реггі"];

let showArr = () => {
  let a = styles.join(", ");
  document.write(`${a}<br>`);
};

// Створіть масив styles з елементами «Джаз» та «Блюз».
let styles = ["Джаз", "Блюз"];
showArr();

// Додайте "Рок-н-рол" в кінець.
styles.push(plusArr[0]);
showArr();

//  Замініть значення всередині на «Класика».
// Ваш код для пошуку значення всередині повинен працювати для масивів з будь - якою довжиною.
let middleChange = (...args) => {
  let a = Math.trunc(args[0].length / 2);
  args[0][a] = args[1][1];
};
middleChange(styles, plusArr);
showArr();

// Видаліть перший елемент масиву та покажіть його.
let firstEl = styles.shift();
showArr();
document.write(`${firstEl}<br>`);

// Вставте «Реп» та «Реггі» на початок масиву.
styles.unshift("Реп", "Реггі");
showArr();
